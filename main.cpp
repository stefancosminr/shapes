#include<iostream>
#include"shapes.h"
#include<vector>
int main()
{ 
  std::vector<Shape*> shapes;
  shapes.push_back(new Circle(10));
  shapes.push_back(new Square(5));
  shapes.push_back(new Rectangle(5,10));
  for(unsigned i=0;i<shapes.size();++i)
    std::cout<<"area of shape "<<i<<" is "
             <<shapes[i]->area()<<'\n';
  for(Shape* s : shapes)
    delete s;
  shapes.clear();
 

}
